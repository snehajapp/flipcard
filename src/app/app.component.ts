import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Flipcard';
  flip = false;

  data = [
    {
      front: {
        imagepath: '/assets/jpg/female-1.jpg'
      },
      back: {
        name: 'Mae Jemison',
        profession: 'Astronaut',
        about: 'Notable women in a space science'
      }
    },
    {
      front: {
        imagepath: '/assets/jpg/female-2.jpg'
      },
      back: {
        name: 'Eileen Collins',
        profession: 'Astronaut',
        about: 'Responsible for Orbiter prelaunch checkout'
      }
    },

    {
      front: {
        imagepath: '/assets/jpg/female-3.jpg'

      },
      back: {
        name: 'Kalpana ChawlaC',
        profession: 'Astronaut',
        about: 'The Successful Maiden Flight-Mission Of Space'
      }
    },

    {
      front: {
        imagepath: '/assets/jpg/female-4.jpg'
      },
      back: {
        name : 'Sunita L. Williams',
        profession: 'Astronaut',
        about: 'Second on the list of total cumulative spacewalk time by a female astronaut'
      }
    },

    {
      front: {
        imagepath: '/assets/jpg/female-5.jpg'
      },
      back: {
        name: 'Barbara Morgan',
        profession: 'Astronaut',
        about: 'American teacher and a former NASA astronaut'
      }
    },

    {
      front: {
        imagepath:'/assets/jpg/female-6.jpg'
      },
      back: {
        name: 'Pamela Melroy',
        profession: 'Astronaut',
        about: 'Astronaut support duties for launch and landing'
      }
    },

    {
      front: {
        imagepath:'/assets/jpg/female-7.jpg'
      },
      back: {
        name: 'Dr. Roberta Bondar',
        profession: 'Astronaut',
        about: 'First neurologist in space'
      }
    },

    {
      front: {
        imagepath:'/assets/jpg/female-8.jpg'
      },
      back: {
        name: 'Karen L. Nyberg',
        profession: 'Astronaut',
        about: 'Mission Specialist'
      }
    },

    {
      front: {
        imagepath:'/assets/jpg/female-9.jpg'
      },
      back: {
        name: 'Nicole Stott',
        profession: 'Astronaut',
        about: ' American engineer '
      }
    },

    {
      front: {
        imagepath:'/assets/jpg/female-10.jpg'
      },
      back: {
        name: 'Lisa Nowak',
        profession: 'Astronaut',
        about: ' Mission specialist in robotics'
      }
    }
  ];


  index = 0;

  constructor() {

  }

  flipCard() {
    console.log('card clicked');
    if (this.flip) {
      this.flip = false;
    } else {
      this.flip = true;
    }
  }

  next() {
    if (this.data && this.data.length - 1 !== this.index) {
      this.index++;
    }else{
      alert('lost card');
    }

  }

  previous() {
    if(this.index !==0 ){
      this.index--;
    }else{
      alert('First card');
    }
  }

  shuffle() {
  this.index = this.pickRandom(1,this.data.length)
  }

  pickRandom(min,max){
    return Math.floor(Math.random()*(max-min+1)+min);  
  }
}
